import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AccessPageHistoryPage } from './access-page-history.page';

const routes: Routes = [
  {
    path: '',
    component: AccessPageHistoryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AccessPageHistoryPage]
})
export class AccessPageHistoryPageModule {}
