import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'my-pending-approval', pathMatch: 'full' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  {path:"my-pending-approval",children:[
    {path:"",loadChildren: './my-pending-approval/my-pending-approval.module#MyPendingApprovalPageModule'},
    {path:""}
  ]},
  { path: 'my-pending-approval', loadChildren: './my-pending-approval/my-pending-approval.module#MyPendingApprovalPageModule' },
  { path: 'access-request-page/:id', loadChildren: './my-pending-approval/access-request-page/access-request-page.module#AccessRequestPagePageModule' },
  { path: 'dash-board', loadChildren: './dash-board/dash-board.module#DashBoardPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
