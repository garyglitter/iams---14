import { Component,OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {  Router, NavigationEnd } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit {

  navLinksArray = [];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router :Router
  ) { 
    this.initializeApp();
    this.router.events.subscribe(events =>{
      const url = this.router.url
      if (event instanceof NavigationEnd){
        const isCurrentUrlSaved = this.navLinksArray.find((item) =>{ return item === url});
        if (!isCurrentUrlSaved) this.navLinksArray.push(url);
      }
    })
    this.hardwareBackButton();
    this.platform.backButton.subscribe(async()=>{
      if(this.router.isActive('/dash-board',true) && this.router.url === '/dash-board'){
          navigator['app'].exitApp();
      }
    })
  };
  ngOnInit() { }
 
  hardwareBackButton(){
    this.platform.backButton.subscribe(() =>{
      if (this.navLinksArray.length > 1){
      this.navLinksArray.pop();
      const index = this.navLinksArray.length + 1;
      const url = this.navLinksArray[index];
      this.router.navigate([url])
      }
      })
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
