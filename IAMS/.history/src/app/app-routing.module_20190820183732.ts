import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { LoginAuthService } from './login/login-auth.service';

const routes: Routes = [
  { path: "", redirectTo: "login", pathMatch: "full" },

  { path: "login", loadChildren: "./login/login.module#LoginPageModule" },
  { path: 'my-pending-approval', loadChildren: './my-pending-approval/my-pending-approval.module#MyPendingApprovalPageModule' },
  { path: 'access-request-page/::business', loadChildren: './my-pending-approval/access-request-page/access-request-page.module#AccessRequestPagePageModule' },
  { path: 'dash-board', loadChildren: './dash-board/dash-board.module#DashBoardPageModule' },
  { path: 'track-request', loadChildren: './track-request/track-request.module#TrackRequestPageModule' },
  { path: 'my-request-approved', loadChildren: './track-request/my-request-approved/my-request-approved.module#MyRequestApprovedPageModule' },
  { path: 'access-page-history/:id', loadChildren: './track-request/my-request-history/access-page-history/access-page-history.module#AccessPageHistoryPageModule' },
  { path: 'access-page-approved/:id', loadChildren: './track-request/my-request-approved/access-page-approved/access-page-approved.module#AccessPageApprovedPageModule' },
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
  { path: 'my-profile', loadChildren: './profile/my-profile/my-profile.module#MyProfilePageModule' },
  { path: 'manage-password', loadChildren: './profile/manage-password/manage-password.module#ManagePasswordPageModule' },
  { path: 'edit-profile', loadChildren: './profile/my-profile/edit-profile/edit-profile.module#EditProfilePageModule' },
  { path: 'forget-password', loadChildren: './profile/manage-password/forget-password/forget-password.module#ForgetPasswordPageModule' },
  { path: 'access-queue/:id', loadChildren: './my-pending-approval/access-queue/access-queue.module#AccessQueuePageModule' }
            
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
