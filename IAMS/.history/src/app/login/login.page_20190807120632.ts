import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  ReactiveFormsModule
} from "@angular/forms";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { LoginAuthService } from './login-auth.service';
@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  submitted = false;
  loginForm: FormGroup;
  passwordType: string = "password";
  passwordIcon: string = "eye-off";
  username:string;
  password:string;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public alertController: AlertController,
    private loginService :LoginAuthService
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required]]
    });
  }

  onSubmit(value: any) {
    
    var dummy_response = {
      username: "gary@g",
      password: "gary"
    };

    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }
    if (
      this.loginService.dummy_response.username == value.username &&
      this.loginService.dummy_response.password == value.password
    ) {
      
      this.router.navigateByUrl("/dash-board");
    } 
    else {
      this.presentAlert();
    }
  }

  get frm() {
    return this.loginForm.controls;
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === "text" ? "password" : "text";
    this.passwordIcon = this.passwordIcon === "eye-off" ? "eye" : "eye-off";
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      message: "UserName/Password is invalid",
      buttons: ["DONE"]
    });

    await alert.present();
  }
}
