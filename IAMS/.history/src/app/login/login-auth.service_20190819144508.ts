import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { Platform } from '@ionic/angular';
import { Storage } from "@ionic/storage";
@Injectable({
  providedIn: 'root'
})
export class LoginAuthService implements CanActivate {

  authenticationState = new BehaviorSubject(false);
  constructor(private router: Router,private storage: Storage,
    private platform: Platform) { }
      canActivate(route: ActivatedRouteSnapshot): boolean {

        console.log(route);

        let authInfo = {
            authenticated: true
        };

        if (!authInfo.authenticated) {
            this.router.navigate(['login']);
            return false;
        }

        return true;

    }
    checkToken()

}
