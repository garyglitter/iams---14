import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot } from "@angular/router";
@Injectable({
  providedIn: "root"
})
export class LoginAuthService implements CanActivate {
  constructor(private router: Router) {}

  dummy_response = {
    username: "gary@g",
    password: "gary"
  };
  authInfo: boolean;
  canActivate(route: ActivatedRouteSnapshot): boolean {
   

    if (!this.authInfo) {
      this.router.navigateByUrl("/dash-board");
    } else {
      this.router.navigateByUrl("/login");
      return true;
    }
    // if (!authInfo.authenticated) {
    //     this.router.navigate(['login']);
    //     return false;
    // }

    return false;
  }
}
