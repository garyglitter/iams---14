import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  ReactiveFormsModule
} from "@angular/forms";
import { Router } from "@angular/router";
@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  submitted = false;
  loginForm: FormGroup;
  passwordType: string = "password";
  passwordIcon: string = "eye-off";

  constructor(private formBuilder: FormBuilder, private router: Router) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required]]
    });
  }
  var dummyCredentials ={
    
  }
  onSubmit(value: any) {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }
   
    this.router.navigateByUrl("/dash-board");
  }


  get frm() {
    return this.loginForm.controls;
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === "text" ? "password" : "text";
    this.passwordIcon = this.passwordIcon === "eye-off" ? "eye" : "eye-off";
  }
}
