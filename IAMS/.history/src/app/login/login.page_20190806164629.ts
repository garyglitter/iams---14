import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {


  submitted = false;
  loginForm : FormGroup;
  

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
   this.loginForm = this.formBuilder.group({
     username : ['',[Validators.required]],
     password : ['',[Validators.required]]
   })
  }
  onClick(){



  }
}
