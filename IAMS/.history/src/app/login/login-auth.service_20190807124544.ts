import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class LoginAuthService implements CanActivate {

  constructor(private router: Router) { 
    this.router.events.subscribe((res) => { 
      if(this.router.url ==="/login"){

      }
      console.log(this.router.url,"Current URL");
  })
  }

   dummy_response = {
    username: "gary@g",
    password: "gary"
  };
  authInfo = {
    authenticated: true
};

  canActivate(route: ActivatedRouteSnapshot): boolean {


    console.log(route);

    if (!this.authInfo.authenticated) {
        this.router.navigate(['login']);
        return false;
    }

    return true;

}
}
