import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class LoginAuthService implements CanActivate {

  constructor(private router: Router) { }

}
