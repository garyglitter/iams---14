import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Platform } from '@ionic/angular';
@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.page.html',
  styleUrls: ['./dash-board.page.scss'],
})
export class DashBoardPage implements OnInit, OnDestroy, AfterViewInit {

  subscription

  constructor(private statusBar: StatusBar,private platform: Platform) {
    this.statusBar.overlaysWebView(true);
    this.statusBar.backgroundColorByHexString('#1761A0');
   }

  ngOnInit() {
 
    this.platform.ready().then(() => {
      //back button handle
      //Registration of push in Android and Windows Phone
      var lastTimeBackPress = 0;
      var timePeriodToExit  = 2000;
      this.platform.registerBackButtonAction(() => {
        let ref = this.viewCtrl.pageRef();
          console.log('Check your needed page ref name', ref.nativeElement.localName) 
          if(ref.nativeElement.localName =='page-no-internet'){
              //Double check to exit app
               if (new Date().getTime() - lastTimeBackPress < timePeriodToExit) {
                   this.platform.exitApp(); //Exit from app
               } else {
                   let toast = this.toastCtrl.create({
                       message:  'Press back again to exit App?',
                       duration: 3000,
                       position: 'bottom'
                   });
                   toast.present();
                   lastTimeBackPress = new Date().getTime();
               }
          }else{
               this.nav.pop();
          }
    });
}
 
  }
  ngAfterViewInit() { 

  }
  ngOnDestroy() {
    
   }
   ionViewDidEnter(){
    this.subscription = this.platform.backButton.subscribe(()=>{
        navigator['app'].exitApp();
    });
}

ionViewWillLeave(){
    this.subscription.unsubscribe();
}
  tap(){
    console.log("printf")
  }


}
