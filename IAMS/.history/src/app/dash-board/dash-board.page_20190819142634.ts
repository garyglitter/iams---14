import { Component, OnInit, OnDestroy, AfterViewInit } from "@angular/core";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { Platform } from "@ionic/angular";
import { AlertController } from "@ionic/angular";
import {  Router } from '@angular/router';

@Component({
  selector: "app-dash-board",
  templateUrl: "./dash-board.page.html",
  styleUrls: ["./dash-board.page.scss"]
})
export class DashBoardPage implements OnInit, OnDestroy, AfterViewInit {
  constructor(
    private statusBar: StatusBar,
    public alertController: AlertController,
    private platform: Platform
  ) {
    this.statusBar.overlaysWebView(true);
    this.statusBar.backgroundColorByHexString("#1761A0");

  
  }

  ngOnInit() {}
  ngAfterViewInit() {
    this.platform.backButton.subscribe
  }
  ngOnDestroy() {}

  tap() {
    console.log("printf");
  }
}
