import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Platform, IonRouterOutlet } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.page.html',
  styleUrls: ['./dash-board.page.scss'],
})
export class DashBoardPage implements OnInit, OnDestroy, AfterViewInit {

  
  @ViewChild(IonRouterOutlet) routerOutlet: IonRouterOutlet;
  constructor(private statusBar: StatusBar,private platform: Platform,private router: Router) {
    this.statusBar.overlaysWebView(true);
    this.statusBar.backgroundColorByHexString('#1761A0');
    
    this.platform.backButton.subscribeWithPriority(0, () => {
      if (this.routerOutlet && this.routerOutlet.canGoBack()) {
        this.routerOutlet.pop();
      } else if (this.router.url === '/LoginPage') {
        this.platform.exitApp(); 
  
        // or if that doesn't work, try
        navigator['app'].exitApp();
      } else {
        this.generic.showAlert("Exit", "Do you want to exit the app?", this.onYesHandler, this.onNoHandler, "backPress");
      }
    });
   }

  ngOnInit() {
 
 
  }
  ngAfterViewInit() { 

  }
  ngOnDestroy() {
    
   }



  tap(){
    console.log("printf")
  }


}
