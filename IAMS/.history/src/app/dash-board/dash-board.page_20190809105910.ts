import { Component, OnInit, OnDestroy, AfterViewInit } from "@angular/core";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { Platform } from "@ionic/angular";
import { AlertController } from "@ionic/angular";
import {  Router } from '@angular/router';
import { async } from 'q';
@Component({
  selector: "app-dash-board",
  templateUrl: "./dash-board.page.html",
  styleUrls: ["./dash-board.page.scss"]
})
export class DashBoardPage implements OnInit, OnDestroy, AfterViewInit {
  constructor(
    private statusBar: StatusBar,
    private platform: Platform,
    public alertController: AlertController,
    private router : Router
  ) {
    this.statusBar.overlaysWebView(true);
    this.statusBar.backgroundColorByHexString("#1761A0");
    this.platform.backButton.subscribe(async()=>{
      if(this.router.isActive('/dash-board',true) && this.router.url === '/dash-board')
    })
  }

  ngOnInit() {}
  ngAfterViewInit() {}
  ngOnDestroy() {}

  tap() {
    console.log("printf");
  }
}
