import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Platform } from '@ionic/angular';
@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.page.html',
  styleUrls: ['./dash-board.page.scss'],
})
export class DashBoardPage implements OnInit, OnDestroy, AfterViewInit {

   backButtonSubscription; 
  public unsubscribeBackEvent: any;

  constructor(private statusBar: StatusBar,private platform: Platform) {
    this.statusBar.overlaysWebView(true);
    this.statusBar.backgroundColorByHexString('#1761A0');
   }

  ngOnInit() {
    // this.initializeBackButtonCustomHandler();
    this.ngAfterViewInit();
  }
  ngAfterViewInit() { 
  //  this.backButtonSubscription = this.platform.backButton.subscribe(()=>{
  //   navigator['app'].exitApp();
  //  });
  }
  ngOnDestroy() {
    this.backButtonSubscription.unsubscribe();
   }
   
  tap(){
    console.log("printf")
  }
  backButtonEvent(){
    this.platform.backButton.subscribe(()=>{
      console.log ('exit');
      navigator['app'].exitApp();
    })
  }
  // ionViewWillLeave() {
    
  //   this.unsubscribeBackEvent && this.unsubscribeBackEvent();
  // }

  // initializeBackButtonCustomHandler(): void {
  //   this.unsubscribeBackEvent = this.platform.backButton.subscribeWithPriority(999999,  () => {
  //       alert("back pressed home" + this.constructor.name);
  //   });
   
  // }
}
