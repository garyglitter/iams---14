import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Platform } from '@ionic/angular';
@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.page.html',
  styleUrls: ['./dash-board.page.scss'],
})
export class DashBoardPage implements OnInit, OnDestroy, AfterViewInit {

   backButtonSubscription; 
  public unsubscribeBackEvent: any;

  constructor(private statusBar: StatusBar,private platform: Platform) {
    this.statusBar.overlaysWebView(true);
    this.statusBar.backgroundColorByHexString('#1761A0');
   }

  ngOnInit() {
 
 
  }
  ngAfterViewInit() { 

  }
  ngOnDestroy() {
    this.backButtonSubscription.unsubscribe();
   }
   
  tap(){
    console.log("printf")
  }


}
