import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RequestServiceService } from 'src/app/my-pending-approval/request-service.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-access-page-approved',
  templateUrl: './access-page-approved.page.html',
  styleUrls: ['./access-page-approved.page.scss'],
})
export class AccessPageApprovedPage implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private requestService: RequestServiceService,
    public alertController: AlertController
  ) {}

  accessType: string;
  transactionNo: string;
  dateAndTime: string;
  description: string;
  ngOnInit() {
  
    this.route.params.subscribe(params => {
      let id = +params["id"];
     this.requestService.dummyData.subscribe(data => {
        data.filter(value => {
         if(value.id===id){
        //   this.accessType = value.accessType
        //   this.transactionNo = value.transactionNumber
        //   this.dateAndTime = value.dateAndTime
        //   this.description = value.description
        //  }
        });
      });
   
    });
 
  }

  
}
