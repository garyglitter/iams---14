import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-my-request-approved',
  templateUrl: './my-request-approved.page.html',
  styleUrls: ['./my-request-approved.page.scss'],
})
export class MyRequestApprovedPage implements OnInit {

  count = [1, 2, 3];

  expandContent: Boolean = true;

  visible: boolean = false;

  dummyData: Observable<PendingRequest[]>;

  fakeData :any;

  constructor(
    private http: HttpClient,
    private requestService: RequestServiceService
  ) {}

  ngOnInit() {
    this.dummyData = this.requestService.dummyData;
    this.requestService.loadAllRequest();

    this.requestService.fakeData().subscribe(data=>{
      this.fakeData = data
    })
  }

  toggle() {
    this.visible = !this.visible;
  }
}
