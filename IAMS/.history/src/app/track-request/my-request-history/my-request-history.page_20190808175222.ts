import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-request-history',
  templateUrl: './my-request-history.page.html',
  styleUrls: ['./my-request-history.page.scss'],
})
export class MyRequestHistoryPage implements OnInit {

  count = [1, 2, 3];

  expandContent: Boolean = true;

  visible: boolean = false;

  dummyData: Observable<PendingRequest[]>;

  fakeData :any;

  constructor(
    private http: HttpClient,
    private requestService: RequestServiceService
  ) {}

  ngOnInit() {
    this.dummyData = this.requestService.dummyData;
    this.requestService.loadAllRequest();

    this.requestService.fakeData().subscribe(data=>{
      this.fakeData = data
    })
  }

  toggle() {
    this.visible = !this.visible;
  }
}
