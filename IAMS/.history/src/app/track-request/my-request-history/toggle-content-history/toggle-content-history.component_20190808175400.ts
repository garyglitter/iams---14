import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PendingRequest } from 'src/app/my-pending-approval/pending-request';
import { HttpClient } from '@angular/common/http';
import { RequestServiceService } from 'src/app/my-pending-approval/request-service.service';

@Component({
  selector: 'toggle-content-history',
  templateUrl: './toggle-content-history.component.html',
  styleUrls: ['./toggle-content-history.component.scss'],
})
export class ToggleContentHistoryComponent implements OnInit {


  @Input() show : Boolean;

  visible: boolean = false;

 
  constructor(private route: ActivatedRoute,
    private requestService: RequestServiceService) { }

  ngOnInit() {
   
  }


  toggle() {
    this.visible = !this.visible;
  }
}
