import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RequestServiceService } from 'src/app/my-pending-approval/request-service.service';

@Component({
  selector: 'app-access-page-history',
  templateUrl: './access-page-history.page.html',
  styleUrls: ['./access-page-history.page.scss'],
})
export class AccessPageHistoryPage implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private requestService: RequestServiceService
  ) {}

  accessType: string;
  transactionNo: string;
  dateAndTime: string;
  description: string;
  ngOnInit() {
  
    this.route.params.subscribe(params => {
      let id = +params["id"];
     this.requestService.dummyData.subscribe(data => {
        data.filter(value => {
        //  if(value.id===id){
        //   this.accessType = value.accessType
        //   this.transactionNo = value.transactionNumber
        //   this.dateAndTime = value.dateAndTime
        //   this.description = value.description
        //  }
        });
      });
   
    });
 
  }

  
}
