import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TrackRequestPage } from './track-request.page';

const routes: Routes = [
  {
    path: '',
    component: TrackRequestPage,
    children:[
      {
        path:'requestHistory',
        children
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TrackRequestPage]
})
export class TrackRequestPageModule {}
