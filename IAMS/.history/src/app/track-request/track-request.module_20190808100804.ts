import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TrackRequestPage } from './track-request.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TrackRequestPage,
    children:[
      {
        path: 'schedule',
        children:[
          {
            path:"",
            loadChildren:"../my-request-history/my-request-history.module#TrackRequestPage"
          }
        ]
      }
    ]
  },
  {
    path : '',
    redirectTo:'',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TrackRequestPage]
})
export class TrackRequestPageModule {}
