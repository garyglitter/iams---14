import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { TrackRequestPage } from "./track-request.page";
import { MyPendingApprovalPageModule } from '../my-pending-approval/my-pending-approval.module';
import { ToggleContentComponent } from './toggle-content/toggle-content.component';

const routes: Routes = [
  {
    path: "",
    component: TrackRequestPage,
    children: [
      {
        path: "requestHistory",
        children: [
          {
            path: "",
            loadChildren:
              "./my-request-history/my-request-history.module#MyRequestHistoryPageModule"
          }
        ]
      },
      {
        path: "requestApproved",
        children: [
          {
            path: "",
            loadChildren:
              "./my-request-approved/my-request-approved.module#MyRequestApprovedPageModule"
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  exports:[ToggleContentComponent],
  declarations: [TrackRequestPage,ToggleContentComponent]
})
export class TrackRequestPageModule {}
