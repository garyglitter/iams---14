import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-track-request-page',
  templateUrl: './track-request-page.page.html',
  styleUrls: ['./track-request-page.page.scss'],
})
export class TrackRequestPagePage implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private requestService: RequestServiceService,
    public alertController: AlertController
  ) {}

  accessType: string;
  transactionNo: string;
  dateAndTime: string;
  description: string;
  ngOnInit() {
    //this.requestService.loadAllRequest();
    this.route.params.subscribe(params => {
      let id = +params["id"];
     this.requestService.dummyData.subscribe(data => {
        data.filter(value => {
         if(value.id===id){
          this.accessType = value.accessType
          this.transactionNo = value.transactionNumber
          this.dateAndTime = value.dateAndTime
          this.description = value.description
         }
        });
      });
   
    });
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      
      message: 'Are you sure want to PushBack?',
      buttons: ['CANCEL','YES']
    });

    await alert.present();
  }


}
