import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProfilePage } from './profile.page';

const routes: Routes = [
  {
    path: "",
    component: ProfilePage,
    children: [
      {
        path: "myProfile",
        children: [
          {
            path: "",
            loadChildren:
              "./my-profile/my-profile.module#MyProfilePageModule"
          }
        ]
      },
      {
        path: "managePassword",
        children: [
          {
            path: "",
            loadChildren:
              "./manage-password/"
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProfilePage]
})
export class ProfilePageModule {}
