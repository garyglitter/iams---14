import { Component, OnInit } from "@angular/core";
import { PhotoServiceService } from "src/app/commonService/photo-service.service";
import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";
import { Storage } from "@ionic/storage";
@Component({
  selector: "app-my-profile",
  templateUrl: "./my-profile.page.html",
  styleUrls: ["./my-profile.page.scss"]
})
export class MyProfilePage implements OnInit {
  fakeImage: boolean;
  photos;
  constructor(
    public photoService: PhotoServiceService,
    private storage: Storage,
    private route : Router,
    private activatedRout :ActivatedRoute
  ) {
    this.activatedRout.params.subscribe(val=>{
      console.log("afctiveted")
      this.ngOnInit();
    })

  }
  ngOnInit() {

 


    this.photoService.loadSaved();
    for (var val of this.photoService.photos) {
      if (val.data) {
        console.log("inner");
        this.fakeImage = true;
      } else {
        this.fakeImage = false;
      }
    }
  
  }

  closeFake() {
  this.route.navigate(['/edit-profile'])
  }
}
