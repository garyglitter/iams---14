import { Component, OnInit } from "@angular/core";
import { PhotoServiceService } from "src/app/commonService/photo-service.service";
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { ToggleContentHistoryComponent } from 'src/app/track-request/my-request-history/toggle-content-history/toggle-content-history.component';

@Component({
  selector: "app-my-profile",
  templateUrl: "./my-profile.page.html",
  styleUrls: ["./my-profile.page.scss"]
})
export class MyProfilePage implements OnInit {

 
  fakeImage: boolean;;
 
 
  constructor(public photoService: PhotoServiceService) {


  }
  ngOnInit() {

    this.photoService.loadSaved();
 
    for(var  val of this.photoService.photos){
      if(val.data){
        console.log("inner")
        this.fakeImage = true;
      }
      else{
        this.fakeImage
      }
    }
  }
  closeFake(){
    this.fakeImage = false;
    console.log("gary")
  }
  
}
