import { Component, OnInit } from "@angular/core";
import { Camera, CameraOptions } from "@ionic-native/camera/ngx";
import { PhotoServiceService } from "src/app/commonService/photo-service.service";
import { BehaviorSubject } from 'rxjs';
@Component({
  selector: "app-my-profile",
  templateUrl: "./my-profile.page.html",
  styleUrls: ["./my-profile.page.scss"]
})
export class MyProfilePage implements OnInit {
  fakeImage: boolean;

  fakeImg = new BehaviorSubject(false);
  constructor(public photoService: PhotoServiceService) {
    if (this.photoService.photos.length > 0) {
      this.fakeImg.next(false);
    }
    // else{
    //   this.fakeImage = true;
    // }
  }
  ngOnInit() {
    this.photoService.loadSaved();
   
  }
  onClick()
  {
    //this.fakeImage = false;
  }
}
