import { Component, OnInit } from "@angular/core";
import { PhotoServiceService } from "src/app/commonService/photo-service.service";
import { Router, NavigationEnd } from "@angular/router";
import { Storage } from "@ionic/storage";
@Component({
  selector: "app-my-profile",
  templateUrl: "./my-profile.page.html",
  styleUrls: ["./my-profile.page.scss"]
})
export class MyProfilePage implements OnInit {
  fakeImage: boolean;
  photos;
  constructor(
    public photoService: PhotoServiceService,
    private storage: Storage,
    private route : Router
  ) {}
  ngOnInit() {
    this.photoService.loadSaved();
   this.route.events.subscribe((events )=>{
      if(events instanceof NavigationEnd){
        this.storage.get("photos").then(photos => {
          this.photos = photos || [];
        });
        
        for (var val of this.photos) {
          if (val.data) {
            console.log("inner");
            this.fakeImage = true;
          } else {
            this.fakeImage = false;
          }
        }
      }
   })


    
 
  
  }

  closeFake() {
    //console.log("edit button pressed");
  }
}
