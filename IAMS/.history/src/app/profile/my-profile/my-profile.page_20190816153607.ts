import { Component, OnInit } from "@angular/core";
import { Camera, CameraOptions } from "@ionic-native/camera/ngx";
import { PhotoServiceService } from "src/app/commonService/photo-service.service";
import { BehaviorSubject } from 'rxjs';
@Component({
  selector: "app-my-profile",
  templateUrl: "./my-profile.page.html",
  styleUrls: ["./my-profile.page.scss"]
})
export class MyProfilePage implements OnInit {
  one: boolean= true;
  imgUrl : string = "../assets/images/profile.png";
  fakeImage: boolean;
  isImgLoaded:boolean = false;
  fakeImg = new BehaviorSubject(false);
  constructor(public photoService: PhotoServiceService) {
    if (this.photoService.photos.length > 0) {
      this.fakeImage = false;
    }
    else{
      this.fakeImage = true;
    }
    this.photoService.loadSaved();
  }
  ngOnInit() {
   
  }
  onClick()
  {
    this.fakeImage = false;
  }
  ionImgDidLoad(){
    
  }
}
