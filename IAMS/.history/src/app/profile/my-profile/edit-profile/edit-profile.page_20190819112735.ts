import { Component, OnInit } from '@angular/core';
import { PhotoServiceService } from 'src/app/commonService/photo-service.service';
import { Storage } from "@ionic/storage";
import { Router } from "@angular/router";
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

  fakeImage: boolean=true;
  constructor(public photoService :PhotoServiceService,private storage: Storage,private route : Router ) {
    my
   }

  ngOnInit() {
   // this.photoService.loadSaved();
 
    for(var  val of this.photoService.photos){
      if(val.data){
        this.fakeImage = false;
      }
    }
  }
  navigate(){
    
    this.route.navigate(['/dash-board'])
  }
  clear(){
  
    this.fakeImage = false;
  }
  updatebut(){
    //this.photoService.loadSaved();
    this.route.navigate(['/dash-board'])
  }

}
