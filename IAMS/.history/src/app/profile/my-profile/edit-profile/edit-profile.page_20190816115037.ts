import { Component, OnInit } from '@angular/core';
import { PhotoServiceService } from 'src/app/commonService/photo-service.service';
import { Storage } from "@ionic/storage";
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  close : boolean;
  fakeImage: boolean = true;
  constructor(public photoService :PhotoServiceService,private storage: Storage) {
    if (this.photoService.photos.length > 0) {
      this.fakeImage = false;
    }
   }

  ngOnInit() {
    this.photoService.loadSaved();
 

  }

  clear(){
    this.storage.clear();
    this.fakeImage = false;
  }
  todo(va){

  }
}
