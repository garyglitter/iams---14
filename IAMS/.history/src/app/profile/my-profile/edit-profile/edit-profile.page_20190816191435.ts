import { Component, OnInit } from '@angular/core';
import { PhotoServiceService } from 'src/app/commonService/photo-service.service';
import { Storage } from "@ionic/storage";
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

  fakeImage: boolean;
  constructor(public photoService :PhotoServiceService,private storage: Storage) {

   }

  ngOnInit() {
    this.photoService.loadSaved();
 
    for(var  val of this.photoService.photos){
      if(val.data){
        this.fakeImage = false;
      }
      else{
        this.fakeImage = true;
      }
    }
  }

  clear(){
  
    this.fakeImage = false;
  }

}
