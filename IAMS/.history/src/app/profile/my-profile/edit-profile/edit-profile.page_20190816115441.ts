import { Component, OnInit } from '@angular/core';
import { PhotoServiceService } from 'src/app/commonService/photo-service.service';
import { Storage } from "@ionic/storage";
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

  fakeImage: boolean;
  constructor(public photoService :PhotoServiceService,private storage: Storage) {
    if (this.photoService.photos.length > 0) {
      this.fakeImage = false;
    }
    else{
      this.fakeImage = true;
    }
   }

  ngOnInit() {
    this.photoService.loadSaved();
 

  }

  clear(){
    this.storage.clear();
    this.fakeImage = false;
  }

}
