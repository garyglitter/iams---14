import { Component, OnInit } from "@angular/core";
import { PhotoServiceService } from "src/app/commonService/photo-service.service";
import { Storage } from "@ionic/storage";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { ImagePicker } from '@ionic-native/image-picker/ngx';
@Component({
  selector: "app-edit-profile",
  templateUrl: "./edit-profile.page.html",
  styleUrls: ["./edit-profile.page.scss"]
})
export class EditProfilePage implements OnInit {
  fakeImage: boolean = true;
  options: any;
  constructor(
    public photoService: PhotoServiceService,
    private storage: Storage,
    private route: Router,
    public alertController: AlertController,
    private imagePicker: ImagePicker
  ) {}

  ngOnInit() {
    this.photoService.loadSaved();
    for (var val of this.photoService.photos) {
      if (val.data) {
        this.fakeImage = false;
      }
    }
  }

  clear() {
    this.fakeImage = false;
  }
  updatebut() {
    //this.photoService.loadSaved();
    this.route.navigate(["/dash-board"]);
  }

  chooseFromgallery(){
    this.options = {

      width: 200,
      //height: 200,
 
      // quality of resized image, defaults to 100
      quality: 25,
 
      // output type, defaults to FILE_URIs.
      // available options are 
      // window.imagePicker.OutputType.FILE_URI (0) or 
      // window.imagePicker.OutputType.BASE64_STRING (1)
      outputType: 1
    };
    this.imageResponse = [];
    this.imagePicker.getPictures(this.options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.imageResponse.push('data:image/jpeg;base64,' + results[i]);
      }
    }, (err) => {
      alert(err);
    });
  }


  async presentAlertMultipleButtons() {
    const alert = await this.alertController.create({
     
      buttons: [
        {
          text: "Take Photo",
          handler: () => {
            this.photoService.openCam();
          }
        },
        {
          text : "Choose From Gallery",
          handler: () => {
           
          }
        }
      ]
    });

    await alert.present();
  }

}
