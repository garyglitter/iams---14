import { Component, OnInit } from "@angular/core";
import { PhotoServiceService } from "src/app/commonService/photo-service.service";
import { Storage } from "@ionic/storage";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { ImagePicker } from '@ionic-native/image-picker/ngx';
@Component({
  selector: "app-edit-profile",
  templateUrl: "./edit-profile.page.html",
  styleUrls: ["./edit-profile.page.scss"]
})
export class EditProfilePage implements OnInit {
  fakeImage: boolean = true;
  options: any;
  imageResponse: any;

  constructor(
    public photoService: PhotoServiceService,
    private storage: Storage,
    private route: Router,
    public alertController: AlertController,
    private imagePicker: ImagePicker
  ) {}

  ngOnInit() {
    this.photoService.loadSaved();
    for (var val of this.photoService.photos) {
      if (val.data) {
        this.fakeImage = false;
      }
    }
  }

  clear() {
    this.fakeImage = false;
  }
  updatebut() {
    //this.photoService.loadSaved();
    this.route.navigate(["/dash-board"]);
  }

 
  // getImages() {
  //   this.options = {
   
  //     width: 200,
    
  //     quality: 25,
 
     
  //     outputType: 1
  //   };
  //   this.imageResponse = [];
  //   this.imagePicker.getPictures(this.options).then((results) => {
  //     for (var i = 0; i < results.length; i++) {
  //       this.imageResponse.push('data:image/jpeg;base64,' + results[i]);
  //     }
  //   }, (err) => {
  //     alert(err);
  //   });
  // }
 

  async presentAlertMultipleButtons() {
    const alert = await this.alertController.create({
     
      buttons: [
        {
          text: "Take Photo",
          handler: () => {
            this.photoService.openCam();
          }
        },
        {
          text : "Choose From Gallery",
          handler: () => {
           this.getImages();
          }
        }
      ]
    });

    await alert.present();
  }

}
