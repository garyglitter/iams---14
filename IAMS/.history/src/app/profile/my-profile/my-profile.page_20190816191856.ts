import { Component, OnInit } from "@angular/core";
import { PhotoServiceService } from "src/app/commonService/photo-service.service";

@Component({
  selector: "app-my-profile",
  templateUrl: "./my-profile.page.html",
  styleUrls: ["./my-profile.page.scss"]
})
export class MyProfilePage implements OnInit {

 
  fakeImage: boolean = true;
 
 
  constructor(public photoService: PhotoServiceService) {


  }
  ngOnInit() {
    this.photoService.loadSaved();
 
    for(var  val of this.photoService.photos){
      if(val.data){
        this.fakeImage = false;
      }
    }
  }
  closeFake(){
    for(var  val of this.photoService.photos){
      if(val.data){
        this.fakeImage = false;
      }
      else{
        
      }
    }
  }
  
}
