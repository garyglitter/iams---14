import { Component, OnInit } from "@angular/core";
import { PhotoServiceService } from "src/app/commonService/photo-service.service";

@Component({
  selector: "app-my-profile",
  templateUrl: "./my-profile.page.html",
  styleUrls: ["./my-profile.page.scss"]
})
export class MyProfilePage implements OnInit {
  fakeImage: boolean;

  constructor(public photoService: PhotoServiceService) {
    this.fakeImage = photoService.fakeImg;
  }
  ngOnInit() {
    this.photoService.loadSaved();

    for (var val of this.photoService.photos) {
      if (val.data) {
        console.log("inner");
        this.photoService.fakeImg = true;
      } else {
        this.photoService.fakeImg = false;
      }
    }
  }
  closeFake() {
    this.photoService.fakeImg = false;
    console.log("gary");
  }
}
