import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PhotoServiceService } from 'src/app/commonService/photo-service.service';
@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.page.html',
  styleUrls: ['./my-profile.page.scss'],
})
export class MyProfilePage implements OnInit {

  constructor(public photoService : PhotoServiceService) { }

  ngOnInit() {
    this.photoService.loadSaved();
    this.photoService.photos.length
  }
  

}
