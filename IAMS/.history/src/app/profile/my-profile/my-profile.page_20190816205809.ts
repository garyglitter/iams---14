import { Component, OnInit } from "@angular/core";
import { PhotoServiceService } from "src/app/commonService/photo-service.service";

@Component({
  selector: "app-my-profile",
  templateUrl: "./my-profile.page.html",
  styleUrls: ["./my-profile.page.scss"]
})
export class MyProfilePage implements OnInit {

 
  //fakeImage: boolean = false;
 
 
  constructor(public photoService: PhotoServiceService) {


  }
  ngOnInit() {

    this.photoService.loadSaved();
 
    for(var  val of this.photoService.photos){
      if(val.data){
        console.log("inner")
        this.photoService.fakeImage = true;
      }
      else{
        this.fakeImage = false;
      }
    }
  }
  closeFake(){
    this.fakeImage = false;
    console.log("gary")
  }
  
}
