import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { LoginAuthService } from './login/login-auth.service';

const routes: Routes = [
  { path: "", redirectTo: "track-request/
  ", pathMatch: "full" },

  { path: "login", loadChildren: "./login/login.module#LoginPageModule" },
  { path: 'my-pending-approval', loadChildren: './my-pending-approval/my-pending-approval.module#MyPendingApprovalPageModule' },
  { path: 'access-request-page/:id', loadChildren: './my-pending-approval/access-request-page/access-request-page.module#AccessRequestPagePageModule' },
  { path: 'dash-board', loadChildren: './dash-board/dash-board.module#DashBoardPageModule',canActivate:[LoginAuthService] },
  { path: 'track-request', loadChildren: './track-request/track-request.module#TrackRequestPageModule' },
  { path: 'my-request-history', loadChildren: './track-request/my-request-history/my-request-history.module#MyRequestHistoryPageModule' },
  { path: 'my-request-approved', loadChildren: './track-request/my-request-approved/my-request-approved.module#MyRequestApprovedPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
