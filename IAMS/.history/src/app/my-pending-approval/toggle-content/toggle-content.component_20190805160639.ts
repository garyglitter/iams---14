import { Component, OnInit, Input } from '@angular/core';
import { RequestServiceService } from '../request-service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'toggle-content',
  templateUrl: './toggle-content.component.html',
  styleUrls: ['./toggle-content.component.scss'],
})
export class ToggleContentComponent implements OnInit {


  @Input() show : Boolean;

  visible: boolean = false;

  data: any;
  constructor(private route: ActivatedRoute,
    private requestService: RequestServiceService) { }

  ngOnInit() {
    console.log(this.show)
  }


  toggle() {
    this.visible = !this.visible;
  }
}
