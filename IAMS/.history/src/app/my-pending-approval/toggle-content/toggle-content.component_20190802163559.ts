import { Component, OnInit, Input } from '@angular/core';
import { RequestServiceService } from '../request-service.service';

@Component({
  selector: 'toggle-content',
  templateUrl: './toggle-content.component.html',
  styleUrls: ['./toggle-content.component.scss'],
})
export class ToggleContentComponent implements OnInit {


  dateAndTime: string;
  visible :boolean = false;

  constructor(  private route: ActivatedRoute,
    private requestService: RequestServiceService) { }

  ngOnInit() {

  }


  toggle(){
    this.visible =! this.visible;
  }
}
