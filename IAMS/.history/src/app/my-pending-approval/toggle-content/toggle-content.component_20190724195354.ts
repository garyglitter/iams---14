import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'toggle-content',
  templateUrl: './toggle-content.component.html',
  styleUrls: ['./toggle-content.component.scss'],
})
export class ToggleContentComponent implements OnInit {

  @Input() accessType :string;
  @Input() transactionNum : string;
  @Input() dateAndTime : string;
  @Input()
  visible :boolean = false;

  constructor() { }

  ngOnInit() {}


  toggle(){
    this.visible =! this.visible;
  }
}
