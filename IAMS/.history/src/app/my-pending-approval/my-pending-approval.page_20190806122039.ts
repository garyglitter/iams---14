import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { RequestServiceService } from "./request-service.service";
import { Observable } from 'rxjs';
import { PendingRequest } from './pending-request';
import { AlertController } from '@ionic/angular';
@Component({
  selector: "app-my-pending-approval",
  templateUrl: "./my-pending-approval.page.html",
  styleUrls: ["./my-pending-approval.page.scss"]
})
export class MyPendingApprovalPage implements OnInit {
  count = [1, 2, 3];

  expandContent: Boolean = true;

  visible: boolean = false;

  dummyData: Observable<PendingRequest[]>;

  fakeData :any;

  constructor(
    private http: HttpClient,
    private requestService: RequestServiceService,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    this.dummyData = this.requestService.dummyData;
    this.requestService.loadAllRequest();

    this.requestService.fakeData().subscribe(data=>{
      this.fakeData = data
    })
  }


  
  toggle() {
    this.visible = !this.visible;
  }


}
