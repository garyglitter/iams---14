import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RequestServiceService } from "../request-service.service";
import { PendingRequest } from "../pending-request";
@Component({
  selector: "app-access-request-page",
  templateUrl: "./access-request-page.page.html",
  styleUrls: ["./access-request-page.page.scss"]
})
export class AccessRequestPagePage implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private requestService: RequestServiceService
  ) {}

  accessType: string;
  transactionNo: string;
  dateAndTime: string;
  description: string;
  dummy;
  ngOnInit() {
    this.requestService.loadAllRequest();
    this.route.params.subscribe(params => {
      let id = params["id"];
      this.requestService.dummyData.subscribe(data => {
        data.filter(value => {
          console.log(value)
        });
      });
      // this.requestService.getRequestByID(id)
      // console.log(this.requestService.getRequestByID(1))
    });
  }
}
