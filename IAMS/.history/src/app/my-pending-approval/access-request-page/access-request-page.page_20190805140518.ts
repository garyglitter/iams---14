import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RequestServiceService } from "../request-service.service";
import { PendingRequest } from "../pending-request";
@Component({
  selector: "app-access-request-page",
  templateUrl: "./access-request-page.page.html",
  styleUrls: ["./access-request-page.page.scss"]
})
export class AccessRequestPagePage implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private requestService: RequestServiceService
  ) {
    setTimeout(() => {
      this.isLoaded = true;
    }, 3000);
  }
  isLoaded = false;
  dummyData = [
    {
      avatar:"https://s3.amazonaws.com/uifaces/faces/twitter/pechkinator/128.jpg",
      name:"Rod Hand",
      address:"Lake Micheleshire, LA 52889-8814"
    },{
      avatar:"https://s3.amazonaws.com/uifaces/faces/twitter/ostirbu/128.jpg",
      name:"Rod Rolfson DVM",
      address:"Delmerchester, OR 94420"
    },{
      avatar:"https://s3.amazonaws.com/uifaces/faces/twitter/jay_wilburn/128.jpg",
      name:"Curt McKenzie",
      address:"North Margaretview, MI 77699-4658"
    },{
      avatar:"https://s3.amazonaws.com/uifaces/faces/twitter/primozcigler/128.jpg",
      name:"Juliet Bogisich",
      address:"North Margaretview, MI 77699-4658"
    },{
      avatar:"https://s3.amazonaws.com/uifaces/faces/twitter/juangomezw/128.jpg",
      name:"Clifton Rowe",
      address:"North Margaretview, MI 77699-4658"
    },{
      avatar:"https://s3.amazonaws.com/uifaces/faces/twitter/dzantievm/128.jpg",
      name:"Vivianne Keebler",
      address:"United States of America"
    },{
      avatar:"https://s3.amazonaws.com/uifaces/faces/twitter/gcmorley/128.jpg",
      name:"Joshua Feest",
      address:"North Margaretview, MI 77699-4658"
    },{
      avatar:"https://s3.amazonaws.com/uifaces/faces/twitter/jay_wilburn/128.jpg",
      name:"Curt McKenzie",
      address:"North Margaretview, MI 77699-4658"
    },{
      avatar:"https://s3.amazonaws.com/uifaces/faces/twitter/primozcigler/128.jpg",
      name:"Juliet Bogisich",
      address:"North Margaretview, MI 77699-4658"
    },{
      avatar:"https://s3.amazonaws.com/uifaces/faces/twitter/juangomezw/128.jpg",
      name:"Clifton Rowe",
      address:"North Margaretview, MI 77699-4658"
    },{
      avatar:"https://s3.amazonaws.com/uifaces/faces/twitter/dzantievm/128.jpg",
      name:"Vivianne Keebler",
      address:"United States of America"
    },{
      avatar:"https://s3.amazonaws.com/uifaces/faces/twitter/gcmorley/128.jpg",
      name:"Joshua Feest",
      address:"North Margaretview, MI 77699-4658"
    }
  ];
  arrayOne(n: number): any[] {
    return Array(n);
  }
  accessType: string;
  transactionNo: string;
  dateAndTime: string;
  description: string;
  ngOnInit() {
    //this.requestService.loadAllRequest();
    this.route.params.subscribe(params => {
      let id = +params["id"];
     this.requestService.dummyData.subscribe(data => {
        data.filter(value => {
         if(value.id===id){
          this.accessType = value.accessType
          this.transactionNo = value.transactionNumber
          this.dateAndTime = value.dateAndTime
          this.description = value.description
         }
        });
      });
   
    });
  }



}
