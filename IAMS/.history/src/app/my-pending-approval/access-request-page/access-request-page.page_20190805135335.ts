import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RequestServiceService } from "../request-service.service";
import { PendingRequest } from "../pending-request";
@Component({
  selector: "app-access-request-page",
  templateUrl: "./access-request-page.page.html",
  styleUrls: ["./access-request-page.page.scss"]
})
export class AccessRequestPagePage implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private requestService: RequestServiceService
  ) {}
data
  accessType: string;
  transactionNo: string;
  dateAndTime: string;
  description: string;
  ngOnInit() {
    //this.requestService.loadAllRequest();
    this.route.params.subscribe(params => {
      let id = +params["id"];
     this.requestService.dummyData.subscribe(data => {
        data.filter(value => {
         if(value.id===id){
          this.accessType = value.accessType
          this.transactionNo = value.transactionNumber
          this.dateAndTime = value.dateAndTime
          this.description = value.description
         }
        });
      });
   
    });
  }


  ionViewWillEnter() {
    setTimeout(() => {
      this.data = {
        'heading': 'Normal text',
        'para1': 'Lorem ipsum dolor sit amet, consectetur',
        'para2': 'adipiscing elit.'
      };
    }, 5000);
  }
}
