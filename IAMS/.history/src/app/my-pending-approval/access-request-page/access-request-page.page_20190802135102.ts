import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RequestServiceService } from '../request-service.service';
@Component({
  selector: "app-access-request-page",
  templateUrl: "./access-request-page.page.html",
  styleUrls: ["./access-request-page.page.scss"]
})
export class AccessRequestPagePage implements OnInit {
  constructor(private route: ActivatedRoute,private requestService : RequestServiceService) {}

  accessType: string;
  transactionNo : string;
  dateAndTime : string;
  description : string; 

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.requestService.getRequestByID()
      console.log(params["id"])
    });

  }
}
