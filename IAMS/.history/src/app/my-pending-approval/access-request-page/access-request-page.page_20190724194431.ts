import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
@Component({
  selector: "app-access-request-page",
  templateUrl: "./access-request-page.page.html",
  styleUrls: ["./access-request-page.page.scss"]
})
export class AccessRequestPagePage implements OnInit {
  constructor(private route: ActivatedRoute) {}

  accessType: string;

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.accessType = params["accessType"];
      console.log(this.accessType)
    });
  }
}
