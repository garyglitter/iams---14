import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { RequestServiceService } from "../request-service.service";
import { PendingRequest } from "../pending-request";
import { AlertController } from '@ionic/angular';
@Component({
  selector: "app-access-request-page",
  templateUrl: "./access-request-page.page.html",
  styleUrls: ["./access-request-page.page.scss"]
})
export class AccessRequestPagePage implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private requestService: RequestServiceService,
    public alertController: AlertController
  ) {}

  transactionNumber : string;
  business :string;
  role :string;
  desiredDate :string;
  purpose :string;

  ngOnInit() {
  
    this.route.params.subscribe(params => {
      let id = +params["id"];
     this.requestService.dummyData.subscribe(data => {
        data.filter(value => {
         if(value.id===id){
            
         }
        });
      });
   
    });
 
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      
      message: 'Are you sure want to PushBack?',
      buttons: ['CANCEL','YES']
    });

    await alert.present();
  }

  
}
