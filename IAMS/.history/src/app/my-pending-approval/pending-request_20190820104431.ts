export class PendingRequest {
    id: number | string;
    requestName: string;
    pendingRequestDetailed : PendingRequestDetailed[]
}
