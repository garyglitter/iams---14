import { Component, OnInit } from "@angular/core";
import { HTTP } from "@ionic-native/http/ngx";
@Component({
  selector: "app-my-pending-approval",
  templateUrl: "./my-pending-approval.page.html",
  styleUrls: ["./my-pending-approval.page.scss"]
})
export class MyPendingApprovalPage implements OnInit {
  count = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  expandContent: Boolean = true;
  constructor(private http: HTTP) {}

  ngOnInit() {
    this.http
    
      .get("http://localhost:3000/details", {}, {})
      .then(data => {
        console.log(data.status);
        console.log(data.data); // data received by server
        console.log(data.headers);
      })
      .catch(error => {
        console.log(error.status);
        console.log(error.error); // error message as string
        console.log(error.headers);
      });
  }

  onClickExpand() {
    this.expandContent = !this.expandContent;
  }
}
