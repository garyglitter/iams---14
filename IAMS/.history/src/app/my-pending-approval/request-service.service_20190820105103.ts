import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { PendingRequest } from './pending-request';
@Injectable({
  providedIn: "root"
})
export class RequestServiceService {

  dummyData : Observable<PendingRequest[]>
  private _dummyData : BehaviorSubject<PendingRequest[]>;
  private baseURL :string;
  private dataStore : {
    dummyData : PendingRequest[];
  };
  constructor(private http: HttpClient) {
    //this.baseURL ="http://www.mocky.io/v2/5d441a372f0000fc0c17949b";
    this.baseURL ="http://www.mocky.io/v2/5d5b814d2f00002b0036faf6";
    this.dataStore = { dummyData : []};
    this._dummyData = <BehaviorSubject<PendingRequest[]>>new BehaviorSubject([]);
    this.dummyData = this._dummyData.asObservable();

    console.log(http://www.mocky.io/v2/5d5b814d2f00002b0036faf6)
   }

   get dummyDatas(){
     return this._dummyData.asObservable();
   }

   loadAllRequest(){
    
    return this.http.get<PendingRequest[]>(this.baseURL).subscribe(data=>{
       this.dataStore.dummyData =data
       this._dummyData.next(Object.assign({},this.dataStore).dummyData);

     },error=>console.log("could not load data"));
  
   }

  // getRequestByID(id){
  //   this.http.get<PendingRequest>(this.baseURL).subscribe(data=>{
  //     let notFound = true;
  //     this.dataStore.dummyData.forEach((request,index)=>{
  //       if(request.id === data.id){
  //         this.dataStore.dummyData[index] =data;
  //         notFound = false;
  //       }
  //     });
  //     if(notFound){
  //       this.dataStore.dummyData.push(data);
  //     }
  //     this._dummyData.next(Object.assign({},this.dataStore).dummyData);
  //   },error => console.log("could not load data"))
  // }
   
  fakeData(){
     return this.http.get<PendingRequest[]>("http://www.mocky.io/v2/5d5b814d2f00002b0036faf6");
  }
}
