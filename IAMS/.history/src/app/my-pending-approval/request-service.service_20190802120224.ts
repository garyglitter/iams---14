import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { PendingRequest } from './pending-request';
@Injectable({
  providedIn: "root"
})
export class RequestServiceService {

  dummyData : Observable<PendingRequest[]>
  private _dummyData : BehaviorSubject<PendingRequest[]>;
  private baseURL :string;
  private dataStore : {
    dummyData : PendingRequest[];
  };
  constructor(private http: HttpClient) {
    this.baseURL ="http://www.mocky.io/v2/5d375c943100006925b078b6";
    this.dataStore = { dummyData : []};
    this._dummyData = <BehaviorSubject<PendingRequest[]>>new BehaviorSubject([]);
    this.dummyData = this._dummyData.asObservable();
   }

   get dummyDatas(){
     return this._dummyData.asObservable();
   }

   loadAllRequest(){
     this.http.get<PendingRequest[]>(this.baseURL).subscribe(data=>{
       this.dataStore.dummyData =data
       this._dummyData.next(Object.assign({},this.dataStore).dummyData);

     },error=>console.log("could not load data"));
   }

   getRequestByID(id){
    this.http.get<PendingRequest[]>(this.baseURL).subscribe(data=>{
      let notFound = true;
      this.dataStore.dummyData.forEach((requestId ,index)=>{
        if(requestId.id === id){
          this.dataStore.dummyData[]
        }
      })
    })
   }
}
