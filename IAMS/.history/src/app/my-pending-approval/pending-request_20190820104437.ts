import { PendingRequestDetailed } from './pending-request-detailed';

export class PendingRequest {
    id: number | string;
    requestName: string;
    pendingRequestDetailed : PendingRequestDetailed[]
}
