export class PendingRequestDetailed {
    transactionNumber : string;
    business : string;
    role :string;
    desiredDate : string;
    purpose :string;
}
