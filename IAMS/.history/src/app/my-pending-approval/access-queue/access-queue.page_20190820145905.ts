import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { RequestServiceService } from "../request-service.service";
@Component({
  selector: 'app-access-queue',
  templateUrl: './access-queue.page.html',
  styleUrls: ['./access-queue.page.scss'],
})
export class AccessQueuePage implements OnInit {


  transactionNum;
  business;
  role;

  constructor(private route: ActivatedRoute,private requestService: RequestServiceService) { }

  ngOnInit() {

    this.route.params.subscribe(params=>{
      let id = +params["id"];
      this.requestService.dummyData.subscribe(data =>{
        data.filter(value =>{
          this.transactionNum = value.
        })
      })
    })
  }

}
