import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { RequestServiceService } from "./request-service.service";
import { Observable } from 'rxjs';
import { PendingRequest } from './pending-request';

@Component({
  selector: "app-my-pending-approval",
  templateUrl: "./my-pending-approval.page.html",
  styleUrls: ["./my-pending-approval.page.scss"]
})
export class MyPendingApprovalPage implements OnInit {
  count = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  expandContent: Boolean = true;

  visible: boolean = false;

  dummyData: Observable<PendingRequest[]>;

  ddd :PendingRequest[];

  constructor(
    private http: HttpClient,
    private requestService: RequestServiceService
  ) {}

  ngOnInit() {
    this.dummyData = this.requestService.dummyData;
    this.requestService.loadAllRequest();
    
  }

  toggle() {
    this.visible = !this.visible;
  }

  test(){
    this.http.get<PendingRequest[]>("http://www.mocky.io/v2/5d441a372f0000fc0c17949b").subscribe(ddd =>{
    this.ddd = ddd
    ()
    })
  }
}
