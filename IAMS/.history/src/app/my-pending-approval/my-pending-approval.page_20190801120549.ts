import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { RequestServiceService } from './request-service.service';
@Component({
  selector: "app-my-pending-approval",
  templateUrl: "./my-pending-approval.page.html",
  styleUrls: ["./my-pending-approval.page.scss"]
})
export class MyPendingApprovalPage implements OnInit {
  count = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  expandContent: Boolean = true;
  
  dummyData: any[];

  loadRequest: Array<any> = new Array(5);

  constructor(private http: HttpClient,private requestService :RequestServiceService) {}

 
  ngOnInit() {
    this.http.get("http://www.mocky.io/v2/5d375c943100006925b078b6").subscribe((data: any[]) => {
      this.dummyData = data;
    });
  }


}
