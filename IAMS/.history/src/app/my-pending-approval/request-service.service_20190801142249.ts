import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { PendingRequest } from './pending-request';
@Injectable({
  providedIn: "root"
})
export class RequestServiceService {

  private dummyValue:PendingRequest[];

  constructor(private http: HttpClient) {
  this.http.get("http://www.mocky.io/v2/5d375c943100006925b078b6").subscribe((data : PendingRequest[])=>{
      this.dummyValue = data;
   });
  }


  // RequestDetails() {
  //   return this.http.get("http://www.mocky.io/v2/5d375c943100006925b078b6").subscribe((data : any[]) =>{
  //       this.dummyValue = data
  //   })
  // }

  // getRequestDetails(){
  //   console.log(this.dummyValue)
  //   return [...this.dummyValue];
  // }


  getValue(){
    test: PendingRequest[]
    return Object.assign([],this.dummyValue);
  }
}
