import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { RequestServiceService } from "./request-service.service";
import { PendingRequest } from "./pending-request";
import { Observable } from 'rxjs';
@Component({
  selector: "app-my-pending-approval",
  templateUrl: "./my-pending-approval.page.html",
  styleUrls: ["./my-pending-approval.page.scss"]
})
export class MyPendingApprovalPage implements OnInit {
  count = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  expandContent: Boolean = true;

  visible :boolean = false;


  dummyData : Observable<PendingRequest[]>;
  
 data : boolean = false;


  constructor(
    private http: HttpClient,
    private requestService: RequestServiceService
  ) {}

  ngOnInit() {


    this.dummyData = this.requestService.dummyData;
    if(this.dummyData){
      console.log("")
    }
    else{

    }
    this.requestService.loadAllRequest();
    
  }
  toggle(){
    
    this.visible =! this.visible;
    
  }

}
