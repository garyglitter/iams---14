import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { PendingRequest } from "./pending-request";
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: "root"
})
export class RequestServiceService {

  dummyData : PendingRequest[];

 

  // RequestDetails() {
  //   return this.http.get("http://www.mocky.io/v2/5d375c943100006925b078b6").subscribe((data : any[]) =>{
  //       this.dummyValue = data
  //   })
  // }
  // getRequestDetails(){
  //   console.log(this.dummyValue)
  //   return [...this.dummyValue];
  // }

  // getRequestValue() {
  //   return this.http.get("http://www.mocky.io/v2/5d375c943100006925b078b6");
  // }

  // getRequestById(id :string){
    
  // }
  private _dummyData : BehaviorSubject<PendingRequest[]>;
  private baseURL :string;
  private dataStore : {
    dummyData : PendingRequest[];
  };
  constructor(private http: HttpClient) {
    this.baseURL ="http://www.mocky.io/v2/5d375c943100006925b078b6";
    this.dataStore = { dummyData : []};
    this._dummyData = <BehaviorSubject<PendingRequest[]>>new BehaviorSubject([]);
   }

   get dummyDatas(){
     return this._dummyData.asObservable();
   }

   loadAllRequest(){
     this.http.get(this.baseURL).subscribe((data)=>{
       this.dataStore.dummyData
       this._dummyData.next(Object.assign({},this.dataStore).dummyData);

     },error=>console.log("could not load data"));
   }

}
