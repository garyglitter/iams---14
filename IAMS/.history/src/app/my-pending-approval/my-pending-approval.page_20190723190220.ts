import { Component, OnInit } from "@angular/core";
import { HttpClient } from '@angular/common/http';
@Component({
  selector: "app-my-pending-approval",
  templateUrl: "./my-pending-approval.page.html",
  styleUrls: ["./my-pending-approval.page.scss"]
})
export class MyPendingApprovalPage implements OnInit {
  count = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  expandContent: Boolean = true;
  dummyData :any[];
  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.http.get<any[]>('http://localhost:3000/details').subscribe(data=>{
      this.dummyData =data;
      console.log(data);
      console.log(this.dummyData[0].accessType)
    })
  }

  onClickExpand() {
    this.expandContent = !this.expandContent;
  }
}
